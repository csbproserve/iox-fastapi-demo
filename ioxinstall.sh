#! /usr/bin/env sh
set -e

docker image save `docker image ls -f "reference=uvicorn-gunicorn-fastapi" -q` > iox-fastapi-demo.tar
ioxclient app install iox_fastapi_demo iox-fastapi-demo.tar
