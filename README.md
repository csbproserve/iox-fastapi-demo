# IOx-FastAPI-DEMO
This project walks you through creating a docker image and deploying it to an IOx platform via the ioxclient. The application is a sample FastAPI Python application that could be used to create a lightweight REST endpoint. These instructions are specific to the ARM64 IOx platforms (ie: IR1101, IE3400). 

This project builds on the work of Sebastian Ramirez at https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker and Emmanuel Tychon at https://github.com/etychon/iox-aarch64-hello-world

### Build Instructions

1. In order to build an ARM64 image on a non ARM dev workstation we need to use the buildx functionality in Docker. This is available in the experimental features of the docker engine and CLI. So, unless you are building on an ARM64 platform, the build machine must have Docker installed and the experimental features enabled. You can confirm you are ready to proceed by entering the following command and verifying that you have the arm64 platform listed:

    ```
    root@localhost# docker buildx ls
    NAME/NODE DRIVER/ENDPOINT STATUS  PLATFORMS
    default * docker
      default default         running linux/amd64, linux/arm64, linux/ppc64le, linux/s390x, linux/386, linux/arm/v7, linux/arm/v6
    ```

2. Download and install the ioxclient for your platform: https://developer.cisco.com/docs/iox/#!iox-resource-downloads

3. Follow the procedure to enable IOx on your platform (IE3x000 listed here):  https://developer.cisco.com/docs/iox/#!ie3x00-series/formatting-an-sd-card-and-enabling-iox

4. Configure the networking profile for the application on the IOx platform (enter in IOS configuration terminal):

    ```
    app-hosting appid iox_fastapi_demo
     app-vnic AppGigabitEthernet trunk
      vlan <#> guest-interface 0
       guest-ipaddress <x.x.x.x> netmask <y.y.y.y>
     app-default-gateway <z.z.z.z> guest-interface 0
    ```
   
5. Run the build.sh script to create the docker image and launch it in your dev environment.

6. Verify that the image is running and that port 80 is mapped to 57633 on your machine:

    ```
    docker ps
    ```
   
7. Verify that you can access the API server in a browser by visiting http://localhost:56733. You should get the output below:
 
    ```json
    {"message":"Hello world! From FastAPI running on Uvicorn with Gunicorn. Using Python 3.8"}
    ``` 

8. Verify that your ioxclient can communicate with the IOx platform you are working with:

   ```
   ioxclient platform info
   ```

9. Run the ioxinstall.sh script to save the docker image to a tar file, install it to the IOx platform. From the IOS terminal activate and start the application using the commands below:

    ```
    app-hosting activate appid iox_fastapi_demo
    app-hosting start appid iox_fastapi_demo
    ```

10. Once the application is running, verify that you can access the API server on the IOx platform by visiting the guest-ipaddress for the application in a browser. It should be available on port 80.

11. If you wish to inspect the running docker container visit the web interface for the IOx platform at https://\<ip of device>/iox/login. You can also ssh to the device and connect a session to the terminal of the docker container:

    ```
    app-hosting connect appid iox_fastapi_demo session /bin/bash
    ```

12. To connect a USB device to the docker container enter the following command into the IOS configuration terminal:

    ```
    app-hosting appid iox_fastapi_demo
     app-device usb-port 1 usb-port-label serial
    ```
