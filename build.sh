#! /usr/bin/env sh
set -e

docker buildx build -t "uvicorn-gunicorn-fastapi:python3.8-alpine" --platform linux/aarch64 .
docker run -d -p 56733:80 --name "uvicorn-gunicorn-fastapi" "uvicorn-gunicorn-fastapi:python3.8-alpine"